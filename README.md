Targeted RNA Knockdown by a Type III CRISPR-Cas Complex in Zebrafish
=================================================
Sequencing data analysis methodology.
-------------------------------------

The analysis has been published in:

> Fricke, Thomas, Dalia Smalakyte, Maciej Lapinski, Abhishek Pateria, Charles Weige, Michal Pastor, Agnieszka Kolano, et al. 2020. “Targeted RNA Knockdown by a Type III CRISPR-Cas Complex in Zebrafish.” The CRISPR Journal 3 (4): 299–313. https://doi.org/10.1089/crispr.2020.0032.

To reach the data pre-processing pipeline sister repository please follow
the link:

https://gitlab.com/lapinskim/csm_sequencing_analysis

**To view the pre-rendered HTLM page documenting the analysis steps follow
the link:**

https://lapinskim.gitlab.io/csm_data_analysis/csm_deseq2.html

### Data availability
All sequencing data have been deposited in the GEO database under accession
number [GSE146852](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE146852).

### Prerequisites

The analysis has been performed in
the [R programing language](https://www.r-project.org/).

The table below lists the required R packages:

| Package           | Version | Date       | Source       |
|-------------------|---------|------------|--------------|
| DESeq2            | 1.24.0  | 2019-05-02 | Bioconductor |
| BiocParallel      | 1.18.1  | 2019-08-06 | Bioconductor |
| ggplot2           | 3.2.1   | 2019-08-10 | CRAN         |
| RColorBrewer      | 1.1-2   | 2014-12-07 | CRAN         |
| pheatmap          | 1.0.12  | 2019-01-04 | CRAN         |
| apeglm            | 1.6.0   | 2019-05-02 | Bioconductor |
| magrittr          | 1.5     | 2014-11-22 | CRAN         |
| Rsamtools         | 2.0.2   | 2019-10-02 | Bioconductor |
| GenomicAlignments | 1.20.1  | 2019-06-18 | Bioconductor |
| IRanges           | 2.18.3  | 2019-09-24 | Bioconductor |
| S4Vectors         | 0.22.1  | 2019-09-09 | Bioconductor |
| Gviz              | 1.28.3  | 2019-09-16 | Bioconductor |
| reshape2          | 1.4.3   | 2017-12-11 | CRAN         |
| clusterProfiler   | 3.12.0  | 2019-05-02 | Bioconductor |
| org.Dr.eg.db      | 3.8.2   | 2019-11-05 | Bioconductor |
| biomaRt           | 2.40.5  | 2019-10-01 | Bioconductor |
| data.table        | 1.12.6  | 2019-10-18 | CRAN         |
| beeswarm          | 0.2.3   | 2016-04-25 | CRAN         |
